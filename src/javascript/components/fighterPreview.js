import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  try {
    fighterElement.append(createFighterImage(fighter), createFighterInfo(fighter));
  } catch (error) {
    console.log('first fighter selected');
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo(fighter) {
  const { name, health, attack, defense } = fighter;
  const infoElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info',
  });

  infoElement.append(createFighterField(fighter, "Name", name));
  infoElement.append(createFighterField(fighter, "Health", health));
  infoElement.append(createFighterField(fighter, "Attack", attack));
  infoElement.append(createFighterField(fighter, "Defense", defense));

  return infoElement;
}

function createFighterField(fighter, label, value) {
  const fieldElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___field',
  });
  fieldElement.innerHTML += label + ': ' + value;

  return fieldElement;
}

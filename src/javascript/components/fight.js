import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstFighterMaxHealth = firstFighter.health;
    const secondFighterMaxHealth = secondFighter.health;
    let firstFighterInBlock = false;
    let secondFighterInBlock = false;
    let someoneInBlock = false;
    let firstFighterCritKeys = initSuperKeys(controls.PlayerOneCriticalHitCombination);
    let secondFighterCritKeys = initSuperKeys(controls.PlayerTwoCriticalHitCombination);
    let firstLastCrit = 0;
    let secondLastCrit = 0;
    let firstCooldown;
    let secondCooldown;

    // attack, start block
    document.addEventListener('keydown', (event) => {
      const keyPressed = 'Key' + event.key.toUpperCase();

      switch (keyPressed) {
        case controls.PlayerOneAttack:
          someoneInBlock = firstFighterInBlock | secondFighterInBlock;
          if (!someoneInBlock) {
            resolveAttack(firstFighter, secondFighter, secondFighterMaxHealth, 'right');
          }
          break;
        case controls.PlayerTwoAttack:
          someoneInBlock = firstFighterInBlock | secondFighterInBlock;
          if (!someoneInBlock) {
            resolveAttack(secondFighter, firstFighter, firstFighterMaxHealth, 'left');
          }
          break;
        case controls.PlayerOneBlock:
          firstFighterInBlock = true;
          break;
        case controls.PlayerTwoBlock:
          secondFighterInBlock = true;
          break;
      }
    });

    // block release
    document.addEventListener('keyup', (event) => {
      const keyPressed = 'Key' + event.key.toUpperCase();

      switch (keyPressed) {
        case controls.PlayerOneBlock:
          firstFighterInBlock = false;
          break;
        case controls.PlayerTwoBlock:
          secondFighterInBlock = false;
          break;
      }
    });

    // superattack
    document.addEventListener('keydown', (event) => {
      const keyPressed = 'Key' + event.key.toUpperCase();

      for (const key of controls.PlayerOneCriticalHitCombination) {
        if (keyPressed == key) {
          firstFighterCritKeys.set(key, true);
          firstCooldown = ((new Date() - firstLastCrit) / 1000) > 10;

          if (!firstFighterInBlock && allValuesTrue(firstFighterCritKeys) && firstCooldown) {
            if (!secondFighterInBlock) {
              resolveCrit(firstFighter, secondFighter, secondFighterMaxHealth, 'right');
            }
            firstLastCrit = new Date();
            firstFighterCritKeys = initSuperKeys(controls.PlayerOneCriticalHitCombination);
          }

        }
      }

      for (const key of controls.PlayerTwoCriticalHitCombination) {
        if (keyPressed == key) {
          secondFighterCritKeys.set(key, true);
          secondCooldown = ((new Date() - secondLastCrit) / 1000) > 10;

          if (!secondFighterInBlock && allValuesTrue(secondFighterCritKeys) && secondCooldown) {
            if (!firstFighterInBlock) {
              resolveCrit(secondFighter, firstFighter, firstFighterMaxHealth, 'left');
            }
            secondLastCrit = new Date();
            secondFighterCritKeys = initSuperKeys(controls.PlayerTwoCriticalHitCombination);
          }
        }
      }
    });

    // superattack release
    document.addEventListener('keyup', (event) => {
      const keyPressed = 'Key' + event.key.toUpperCase();

      for (const key of controls.PlayerOneCriticalHitCombination) {
        if (keyPressed == key) {
          firstFighterCritKeys.set(key, false);
        }
      }

      for (const key of controls.PlayerTwoCriticalHitCombination) {
        if (keyPressed == key) {
          secondFighterCritKeys.set(key, false);
        }
      }
    });

    // endgame
    document.addEventListener('keydown', (event) => {
      if (firstFighter.health < 0) {
        resolve(secondFighter);
      } else if (secondFighter.health < 0) {
        resolve(firstFighter);
      }
    });
  });
}

export function getDamage(attacker, defender) {
  const damage = Math.max(getHitPower(attacker) - getBlockPower(defender), 0)

  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = 1 + Math.random();
  const power = fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  const dodgeChance = 1 + Math.random();
  const power = fighter.defense * dodgeChance;

  return power;
}

function resolveAttack(attacker, defender, defenderMaxHealth, side) {
  defender.health -= getDamage(attacker, defender);
  const healthPercent = 100.0 * defender.health / defenderMaxHealth;
  updateHealthBar(`${side}-fighter-indicator`, healthPercent);
}

function updateHealthBar(id, percent) {
  const healthBar = document.getElementById(id);
  const newWidth = Math.max(0, percent);
  healthBar.style.width = newWidth + "%";
}

function initSuperKeys(keyList) {
  const res = new Map();
  for (const key of keyList) {
    res.set(key, false);
  }

  return res;
}

function allValuesTrue(keyMap) {
  for (const value of keyMap.values()) {
    if (value === false) {
      return false;
    }
  }

  return true;
}

function resolveCrit(attacker, defender, defenderMaxHealth, side) {
  defender.health -= 2 * attacker.attack;
  const healthPercent = 100.0 * defender.health / defenderMaxHealth;
  updateHealthBar(`${side}-fighter-indicator`, healthPercent);
}

import { showModal } from './modal.js';

export function showWinnerModal(fighter) {
  const modalElement = {
    title: "Winner",
    bodyElement: fighter.name + ', ' + Math.ceil(fighter.health) + ' health',
    onClose: () => location.reload(),
  }

  showModal(modalElement);
}
